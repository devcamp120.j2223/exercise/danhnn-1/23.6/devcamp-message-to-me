import { Component } from "react";

import backgroundImg from "../../../assets/images/background.jpg";

class TitleImage extends Component {
    render() {
        return (
            <div className="row mt-2">
                <div className="col-12">
                    <img src={backgroundImg} alt="title" width={500} className="img-thumbnail"/>
                </div>
            </div>
        )
    }
}

export default TitleImage;

