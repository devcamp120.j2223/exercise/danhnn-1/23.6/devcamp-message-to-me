import { Component } from "react";

class TitleText extends Component {
    render() {
        return (
            <div className="row mt-5">
                <div className="col-12">
                    <h1>Chào mừng đến với Devcamp 120</h1>
                </div>
            </div>
        )
    }
}

export default TitleText;
