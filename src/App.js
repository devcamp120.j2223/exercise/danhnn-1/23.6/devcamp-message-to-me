import "bootstrap/dist/css/bootstrap.min.css";

import TitleComponent from "./components/title/TitleComponent";
import ContentComponent from "./components/content/ContentComponent";

function App() {
  return (
    <div className="container text-center">
      <TitleComponent />
      <ContentComponent />
    </div>
  );
}

export default App;
